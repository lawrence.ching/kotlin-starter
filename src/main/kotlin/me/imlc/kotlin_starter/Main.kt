package me.imlc.kotlin_starter

import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.instance
import com.google.common.util.concurrent.AbstractIdleService
import me.imlc.kotlin_starter.api.HealthApi
import me.imlc.kotlin_starter.exts.start
import me.imlc.kotlin_starter.kodein.Kodeins
import me.imlc.kotlin_starter.utils.Common
import org.slf4j.Logger

class Main(kodein: Kodein = Kodeins.DEFAULT): AbstractIdleService() {
    private var logger:Logger = kodein.instance()
    private var httpService:HttpService = kodein.instance()

    init {
        val common = Common()
        val healthApi = HealthApi(common)

        httpService.http.get("health", healthApi::health)
    }

    override fun startUp() {
        logger.info("Starting up services...")
        httpService.start()
    }

    override fun shutDown() {
        logger.info("Shutting down services...")
    }


    companion object {

        @JvmStatic
        fun main(args:Array<String>) {
            val main = Main()
            main.startAsync()
        }
    }
}