package me.imlc.kotlin_starter.core

import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.bind
import com.github.salomonbrys.kodein.singleton
import org.slf4j.LoggerFactory

val DEFAULT_CORE_DOMULE = Kodein.Module {
    bind() from singleton {
        LoggerFactory.getLogger("root")
    }
}
