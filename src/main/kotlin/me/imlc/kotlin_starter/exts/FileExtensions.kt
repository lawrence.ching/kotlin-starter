package me.imlc.kotlin_starter.exts

import java.io.File

fun File.mkdirIfNotExist() {
    if(this.isFile) {
        throw RuntimeException("Cannot create directory because of existing file with same name")
    }

    if(!this.exists()) {
        this.mkdirs()
    }
}