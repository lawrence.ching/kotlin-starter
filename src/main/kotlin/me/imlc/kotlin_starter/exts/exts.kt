package me.imlc.kotlin_starter.exts

import com.google.common.util.concurrent.Service
import org.apache.http.HttpEntity
import org.apache.http.util.EntityUtils
import org.json.JSONObject
import java.util.concurrent.TimeUnit.MINUTES

fun Service.start(timeoutInMinutes: Long = 5) {
    this.startAsync()
    this.awaitRunning(timeoutInMinutes, MINUTES)
}

fun Service.stop(timeoutInMinutes: Long = 5) {
    this.stopAsync()
    this.awaitTerminated(timeoutInMinutes, MINUTES)
}

fun HttpEntity.string(): String? {
    return EntityUtils.toString(this, "UTF-8")
}

fun String.toJson(): JSONObject? {
    return JSONObject(this)
}