package me.imlc.kotlin_starter.utils

open class Common {
    open val now:Long
        get() = System.currentTimeMillis()
}