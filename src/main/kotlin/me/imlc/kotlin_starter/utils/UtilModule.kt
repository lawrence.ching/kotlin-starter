package me.imlc.kotlin_starter.utils

import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.bind
import com.github.salomonbrys.kodein.singleton

var DEFAULT_UTIL_MODULE = Kodein.Module {
    bind() from singleton { Common() }
}