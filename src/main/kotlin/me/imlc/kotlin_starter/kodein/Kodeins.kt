package me.imlc.kotlin_starter.kodein

import com.github.salomonbrys.kodein.Kodein
import me.imlc.kotlin_starter.core.DEFAULT_CORE_DOMULE
import me.imlc.kotlin_starter.utils.DEFAULT_UTIL_MODULE

val PORT = "port"

class Kodeins {

    companion object {
        val DEFAULT = Kodein {
            import(DEFAULT_CORE_DOMULE)
            import(DEFAULT_UTIL_MODULE)
            import(DEFAULT_HTTP_SERVICE_MODULE)
        }
    }
}