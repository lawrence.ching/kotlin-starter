package me.imlc.kotlin_starter.kodein

import com.github.salomonbrys.kodein.*
import me.imlc.kotlin_starter.HttpService

val DEFAULT_HTTP_SERVICE_MODULE = Kodein.Module {
    constant(PORT) with 8080
    bind() from singleton { HttpService(instance(PORT), instance()) }
}
