package me.imlc.kotlin_starter

import com.google.common.util.concurrent.AbstractIdleService
import org.slf4j.Logger
import spark.Service
import spark.Service.ignite
import java.net.URI

class HttpService(
        private val port:Int,
        private val logger: Logger
): AbstractIdleService() {

    val http: Service
    private val baseUrl = "http://localhost:$port"

    init {
        val uri = URI(baseUrl)

        http = ignite()
        http.port(port)
        http.ipAddress(uri.host)
    }

    override fun startUp() {
        logger.info("Server started at $baseUrl")
    }

    override fun shutDown() {
        http.stop()
    }
}