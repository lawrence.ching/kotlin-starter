package me.imlc.kotlin_starter.api

import me.imlc.kotlin_starter.utils.Common
import org.json.JSONObject
import spark.Request
import spark.Response
import java.util.concurrent.TimeUnit

class HealthApi(private val common: Common) {

    private val startUpTime = common.now

    private fun upTime(): String {
        val duration = common.now - startUpTime
        val hours = TimeUnit.MILLISECONDS.toHours(duration)
        val minutes = TimeUnit.MILLISECONDS.toMinutes(duration) - TimeUnit.HOURS.toMinutes(hours)
        val seconds = TimeUnit.MILLISECONDS.toSeconds(duration) - TimeUnit.HOURS.toSeconds(hours) - TimeUnit.MINUTES.toSeconds(minutes)
        return String.format("%02d:%02d:%02d", hours, minutes, seconds)
    }

    fun health(req: Request, res: Response): String {
        res.type("application/json")

        val health = JSONObject()

        health.put("isAvailable", true)
        health.put("application", "kotlin-starter")
        health.put("upTime", upTime())
        health.put("Author", "Lawrence Ching")

        return health.toString()
    }

}