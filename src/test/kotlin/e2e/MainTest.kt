package e2e

import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.instance
import com.github.salomonbrys.kodein.with
import me.imlc.kotlin_starter.HttpService
import me.imlc.kotlin_starter.Main
import me.imlc.kotlin_starter.exts.start
import me.imlc.kotlin_starter.exts.stop
import me.imlc.kotlin_starter.kodein.Kodeins
import me.imlc.kotlin_starter.kodein.PORT
import org.amshove.kluent.shouldBe
import org.junit.After
import org.junit.Before
import org.junit.Test
import scaffold.getFreePort

class MainTest {
    private var port: Int = 0

    private lateinit var kodein: Kodein

    private lateinit var main: Main

    @Test
    fun canStartHttpService() {
        val httpService: HttpService = kodein.instance()
        httpService.isRunning shouldBe true
    }

    @Before
    fun setUp() {
        port = getFreePort()
        kodein = Kodein {
            extend(Kodeins.DEFAULT)
            constant(PORT, overrides = true) with port
        }
        main = Main(kodein)
        main.start()
    }

    @After
    fun tearDown() {
        main.stop()
    }
}