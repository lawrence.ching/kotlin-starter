package me.imlc.kotlin_starter.api

import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import io.kotlintest.mock.mock
import me.imlc.kotlin_starter.utils.Common
import org.hamcrest.Matchers.equalTo
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Test
import spark.Response
import java.util.concurrent.TimeUnit

class HealthApiTest {

    private lateinit var mockResponse: Response
    private lateinit var mockCommon: Common
    private lateinit var healthApi: HealthApi

    @Before
    fun setUp() {
        mockResponse = mock()
        mockCommon = mock()

        whenever(mockCommon.now).thenReturn(TimeUnit.DAYS.toMillis(0))

        healthApi = HealthApi(mockCommon)
    }

    @Test
    fun canReturnHealthData() {
        whenever(mockCommon.now).then { TimeUnit.DAYS.toMillis(2) }

        val content = healthApi.health(mock(), mockResponse)

        assertThat(content, equalTo("""{"isAvailable":true,"upTime":"48:00:00","application":"kotlin-starter","Author":"Lawrence Ching"}"""))
        verify(mockResponse).type("application/json")
    }
}