package me.imlc.kotlin_starter

import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.instance
import com.github.salomonbrys.kodein.with
import me.imlc.kotlin_starter.api.HealthApi
import me.imlc.kotlin_starter.exts.start
import me.imlc.kotlin_starter.exts.stop
import me.imlc.kotlin_starter.exts.string
import me.imlc.kotlin_starter.kodein.DEFAULT_HTTP_SERVICE_MODULE
import me.imlc.kotlin_starter.kodein.PORT
import org.amshove.kluent.shouldEqualTo
import org.apache.http.client.methods.HttpGet
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.impl.client.HttpClients
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.skyscreamer.jsonassert.JSONAssert
import org.slf4j.Logger
import scaffold.TestModule
import scaffold.getFreePort

class HttpServiceTest {
    private lateinit var httpService: HttpService
    private lateinit var logger: Logger
    private lateinit var httpClient: CloseableHttpClient
    private var port: Int = 8080

    @Test
    fun canAccessHealthApi() {
        val httpGet = HttpGet("http://localhost:$port/health")
        val response = httpClient.execute(httpGet)
        response.statusLine.statusCode shouldEqualTo 200
        response.entity.contentType.value shouldEqualTo "application/json"

        val content = response.entity.string()
        JSONAssert.assertEquals(
                """
                    {
                      "isAvailable": true,
                      "upTime": "00:00:00",
                      "application": "kotlin-starter",
                      "Author": "Lawrence Ching"
                    }
                """.trimIndent(),
                content, false)
    }

    @Before
    fun setUp() {
        port = getFreePort()

        val kodein = Kodein {
            import(TestModule.TEST_CORE_DOMULE)
            import(TestModule.TEST_UTIL_MODULE)
            import(DEFAULT_HTTP_SERVICE_MODULE)
            constant(PORT, overrides = true) with port
        }

        logger = kodein.instance()

        httpService = kodein.instance()

        val healthApi = HealthApi(kodein.instance())

        httpService.http.get("health", healthApi::health)

        httpService.start()

        httpClient = HttpClients.createMinimal()
    }

    @After
    fun tearDown() {
        httpService.stop()
        httpClient.close()
    }
}