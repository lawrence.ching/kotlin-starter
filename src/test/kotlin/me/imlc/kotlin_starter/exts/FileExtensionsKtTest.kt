package me.imlc.kotlin_starter.exts

import io.kotlintest.matchers.shouldThrow
import io.kotlintest.specs.StringSpec
import org.amshove.kluent.shouldBeTrue
import org.amshove.kluent.shouldEqualTo
import org.apache.commons.io.FileUtils
import scaffold.newSandbox
import java.nio.file.Paths

class FileExtensionsKtTest: StringSpec() {
    init {
        "Throw exception if file exists with same name" {
            val sandbox = newSandbox()
            val existingFile = Paths.get(sandbox.path, "expectedDirectoryName").toFile()
            FileUtils.writeStringToFile(existingFile, "some words", "UTF-8")
            existingFile.exists().shouldBeTrue()

            val exception = shouldThrow<RuntimeException> {
                val folder = Paths.get(sandbox.path, "expectedDirectoryName").toFile()
                folder.mkdirIfNotExist()
            }

            exception.message!!.shouldEqualTo("Cannot create directory because of existing file with same name")
        }

        "Can create directory with missing parent folder" {
            val sandbox = newSandbox()

            val folder = Paths.get(sandbox.path, "parent1", "parent2", "targetDir").toFile()
            folder.mkdirIfNotExist()

            folder.exists().shouldBeTrue()
        }
    }
}