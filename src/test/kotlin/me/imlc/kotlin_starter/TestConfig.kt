package me.imlc.kotlin_starter

import io.kotlintest.ProjectConfig
import scaffold.clean as cleanSandboxes

object TestConfig: ProjectConfig() {
    override fun afterAll() {
        cleanSandboxes()
    }
}