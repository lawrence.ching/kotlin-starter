package scaffold

import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.bind
import com.github.salomonbrys.kodein.singleton
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import me.imlc.kotlin_starter.utils.Common
import org.slf4j.LoggerFactory
import java.time.LocalDateTime
import java.time.ZoneOffset

val TEST_NOW_STR = "2016-06-26T09:09:29"
val TEST_NOW = LocalDateTime.parse(TEST_NOW_STR).toInstant(ZoneOffset.UTC).toEpochMilli()

class TestModule {

    companion object {
        private val mockCommon: Common

        init {
            mockCommon = mock<Common>()
            whenever(mockCommon.now).thenReturn(TEST_NOW)
        }

        val TEST_UTIL_MODULE = Kodein.Module {
            bind() from singleton { mockCommon }
        }

        val TEST_CORE_DOMULE = Kodein.Module {

            bind() from singleton {
                LoggerFactory.getLogger("test")
            }

        }
    }
}

