package scaffold

import me.imlc.kotlin_starter.exts.mkdirIfNotExist
import org.apache.commons.io.FileUtils
import java.io.File
import java.nio.file.Paths
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*

private val tmpFolder = Paths.get("tmp").toFile()

fun newSandbox(): File {
    val tmp = mkdirTmpFolderIfNotExist()
    val sandbox = Paths.get(tmp.path, getNameWithTimeStamp()).toFile()
    mkdirIfNotExist(sandbox)
    return sandbox
}

fun clean() {
    FileUtils.deleteDirectory(tmpFolder)
}

private fun getNameWithTimeStamp(): String {
    return LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")) + ' ' + Integer.toHexString(Random().nextInt())
}

private fun mkdirTmpFolderIfNotExist(): File {
    mkdirIfNotExist(tmpFolder)
    return tmpFolder
}

private fun mkdirIfNotExist(file: File) {
    if(!file.exists()) {
        file.mkdir()
    }
}