package scaffold

import java.net.ServerSocket

fun getFreePort(): Int {
    val socket = ServerSocket(0)
    val port = socket.localPort
    socket.close()
    return port
}