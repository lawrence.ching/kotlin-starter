# Kotlin Starter [![pipeline status](https://gitlab.com/lawrence.ching/kotlin-starter/badges/master/pipeline.svg)](https://gitlab.com/lawrence.ching/kotlin-starter/commits/master) [![coverage report](https://gitlab.com/lawrence.ching/kotlin-starter/badges/master/coverage.svg)](https://gitlab.com/lawrence.ching/kotlin-starter/commits/master)

A simple starter project for Kotlin.  
Check all `//TODO` comment to change accordingly.

### Integrated
Logback

kotlin-test

mockito-kotlin

Gradle

Spark(Web Server)

Kodein